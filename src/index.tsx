import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { Main } from "./views";

ReactDOM.render(<Main />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

/**
 * Enable hot reloading when in dev.
 * This does nothing in prod.
 */
// tslint:disable-next-line no-any
declare const module: any;

if (module.hot) {
    module.hot.accept();
}
