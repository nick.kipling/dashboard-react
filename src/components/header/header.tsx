import React from "react";
import styled from "styled-components";

const HeaderContainer = styled.div`
    background: #8259ff; /* Old browsers */
    background: -moz-linear-gradient(
        45deg,
        #8259ff 0%,
        #00e4be 55%,
        #00e4be 99%
    ); /* FF3.6-15 */
    background: -webkit-linear-gradient(
        45deg,
        #8259ff 0%,
        #00e4be 55%,
        #00e4be 99%
    ); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(
        45deg,
        #8259ff 0%,
        #00e4be 55%,
        #00e4be 99%
    ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    height: 250px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-left: 40px;

    h1 {
        text-transform: uppercase;
        font-size: 1.2rem;
        /* letter-spacing: 0.05rem; */
        margin: 0;
    }

    h2 {
        text-transform: uppercase;
        font-size: 3rem;
        margin: 0;
    }
`;

export function Header (): JSX.Element {
    return (
        <HeaderContainer>
            <h1>League of Legends</h1>
            <h2>Champion Stats</h2>
        </HeaderContainer>
    );
}
