import React, { useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { Icon } from "..";
import { icons } from "../../models";
import { Colors } from "../../styles";

const expanded = css`
    position: relative;
    transform: rotate(0deg);
    transform-origin: 0 0;
    margin: 10px 0 0 0;
`;

const TimerContainer = styled.div`
    font-size: 3rem;
    height: 60px;
    position: absolute;
    transform: translateY(-100%) rotate(90deg);
    transform-origin: left bottom;
    margin: 0 -18px 0;
    color: ${Colors.LEC2};

    &.expanded {
        ${expanded}
    }

    @media only screen and (min-width: 800px) {
        ${expanded}
    }
`;

const IconContainer = styled.div`
    padding-top: 12px;
`;

interface TimerProps {
    startFrom: number;
    loading: boolean;
    expanded?: boolean;
    onTimerFinish (): void;
}

/**
 * Timer component.
 * Counts down from the provided start at value.
 * A very minimal countdown timer. Doesn't handle anything longer than 60
 * seconds or provide much ability to customise. Would be a lot better if you
 * set number of loops, warnings at intervals, etc.
 */
export function Timer (props: TimerProps): JSX.Element {
    const [countdown, setCountdown] = useState(props.startFrom);

    useEffect(() => {
        if (!props.loading) {
            const timer: number = setInterval(() => {
                if (countdown === 0) {
                    props.onTimerFinish();
                }

                setCountdown(
                    countdown - 1 > -1 ? countdown - 1 : props.startFrom
                );
            }, 1000);

            return function cleanUp (): void {
                clearInterval(timer);
            };
        }
    });

    const renderTime = (time: number): string => {
        if (time < 10) {
            return `0:0${time}`;
        } else {
            return `0:${time}`;
        }
    };

    return (
        <TimerContainer className={props.expanded ? "expanded" : ""}>
            {props.loading ? (
                <IconContainer>
                    <Icon
                        icon={icons.LoadingSpinner}
                        spin
                        height={36}
                        width={36}
                    />
                </IconContainer>
            ) : (
                renderTime(countdown)
            )}
        </TimerContainer>
    );
}
