import React from "react";
import styled from "styled-components";
import { IconDefinition } from "../../models";

const IconContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    flex: 0;

    svg {
        margin: auto;

        &[data-spin] {
            animation: spin 1s infinite linear;
            transform-origin: 50% 50%;
        }
    }
`;

interface IconProps {
    height?: number;
    width?: number;
    icon: IconDefinition;
    color?: string;
    badge?: number;
    spin?: boolean;
    className?: string;
}

/**
 * Icon component.
 * Renders an SVG icon.
 * This was ripped from an Icon component I've previously built. Some functionality has been
 * removed (i.e. the badges) as they're not used in this challenge.
 */
export function Icon (props: IconProps): JSX.Element | null {
    if (!props.icon) {
        return null;
    }

    const attrs: { "data-badge"?: string; className?: string } = {};

    if (props.badge) {
        attrs["data-badge"] = props.badge.toString();
    }

    if (props.className) {
        attrs.className = props.className;
    }

    return (
        <IconContainer
            style={{ height: props.height, width: props.width, flex: 0 }}
            {...attrs}
        >
            <svg
                height={props.height || "16"}
                width={props.width || "16"}
                viewBox={props.icon.viewBox}
                data-spin={props.spin}
            >
                {props.icon.paths.map((x, idx) => (
                    <path
                        d={x}
                        key={idx}
                        fill={props.color || "currentColor"}
                    />
                ))}
            </svg>
        </IconContainer>
    );
}
