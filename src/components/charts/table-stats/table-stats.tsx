import React from "react";
import styled from "styled-components";
import { ChampRadarData } from "../../../models";
import { Colors } from "../../../styles";

const Table = styled.table`
    border: 1px solid ${Colors.BorderColor};
    border-collapse: collapse;
    flex: 1;

    tr,
    td {
        border: 1px solid ${Colors.BorderColor};
        padding: 10px;
    }
`;

const CenteredCell = styled.td`
    text-align: center;
`;

interface TableStatsProps {
    data: ChampRadarData;
}

export function TableStats (props: TableStatsProps): JSX.Element {
    return (
        <Table>
            <tbody>
                <tr>
                    <td>Avg. Kills</td>
                    <CenteredCell>
                        {props.data ? props.data.datasets[0].data[2] : 0}
                    </CenteredCell>
                </tr>
                <tr>
                    <td>Avg. Deaths</td>
                    <CenteredCell>
                        {props.data ? props.data.datasets[0].data[4] : 0}
                    </CenteredCell>
                </tr>
                <tr>
                    <td>Avg. Assists</td>
                    <CenteredCell>
                        {props.data ? props.data.datasets[0].data[3] : 0}
                    </CenteredCell>
                </tr>
                <tr>
                    <td>Win Rate</td>
                    <CenteredCell>
                        {props.data ? props.data.datasets[0].data[0] : 0}%
                    </CenteredCell>
                </tr>
                <tr>
                    <td>Ban Rate</td>
                    <CenteredCell>0.07%</CenteredCell>
                </tr>
            </tbody>
        </Table>
    );
}
