import React from "react";
import { Doughnut } from "react-chartjs-2";
import { GraphContainer } from "..";
import { MostPlayedData } from "../../../models";

export interface MostPlayedProps {
    data: MostPlayedData;
}

export function MostPlayed (props: MostPlayedProps): JSX.Element {
    return (
        <GraphContainer>
            <h3>Most Played Champs</h3>
            <div
                style={{
                    position: "relative",
                    height: "300px",
                    width: "100%",
                    display: "flex",
                    alignItems: "center"
                }}
            >
                <Doughnut
                    data={props.data}
                    height={100}
                    options={{ maintainAspectRatio: false }}
                />
            </div>
            <p>
                The amount of games that each champ has been seen in since games
                were recorded.
            </p>
        </GraphContainer>
    );
}
