import React from "react";
import { Radar } from "react-chartjs-2";
import { ChampRadarData } from "../../../models";

interface ChampRadarProps {
    data: ChampRadarData;
}

export function ChampRadar (props: ChampRadarProps): JSX.Element {
    return (
        <div
            style={{
                position: "relative",
                height: "100%",
                width: "100%",
                display: "flex",
                flex: 1,
                alignItems: "center"
            }}
        >
            <Radar
                data={{ ...props.data }}
                options={{
                    scale: {
                        ticks: {
                            maxTicksLimit: 6,
                            display: false,
                            stepSize: 5
                        }
                    },
                    tooltips: {
                        enabled: false
                    }
                }}
            />
        </div>
    );
}
