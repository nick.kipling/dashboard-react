import React from "react";
import { Bar } from "react-chartjs-2";
import { GraphContainer } from "..";
import { WinLossData } from "../../../models";

interface WinLossProps {
    data: WinLossData;
}

export function WinLoss (props: WinLossProps): JSX.Element {
    return (
        <GraphContainer>
            <h3>Win / Loss %</h3>
            <div
                style={{ position: "relative", height: "300px", width: "100%" }}
            >
                <Bar
                    data={props.data}
                    height={100}
                    options={{
                        maintainAspectRatio: false,
                        scales: {
                            xAxes: [
                                {
                                    gridLines: {
                                        display: false
                                    }
                                }
                            ],
                            yAxes: [
                                {
                                    gridLines: {
                                        display: false
                                    },
                                    ticks: {
                                        stepSize: 20
                                    }
                                }
                            ]
                        }
                    }}
                />
            </div>
            <p>
                The win / loss percentage of each champion over the last
                recorded games.
            </p>
        </GraphContainer>
    );
}
