import styled from "styled-components";

export const GraphContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;

    p {
        line-height: 1.6rem;
    }

    h3 {
        text-transform: uppercase;
        font-size: 22px;
        margin: 0 0 20px;
    }
`;
