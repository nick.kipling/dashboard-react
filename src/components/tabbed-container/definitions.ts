import {
    asheChampRadarFake,
    ChampRadarData,
    dravenChampRadarFake
} from "../../models";

export interface ITab {
    img: string;
    label: string;
    selected: boolean;
    data: ChampRadarData;
}

export const tabs: ITab[] = [
    {
        img:
            "https://ddragon.leagueoflegends.com/cdn/9.7.1/img/champion/Ashe.png",
        label: "Ashe",
        selected: true,
        data: asheChampRadarFake
    },
    {
        img:
            "https://ddragon.leagueoflegends.com/cdn/9.7.1/img/champion/Draven.png",
        label: "Draven",
        selected: false,
        data: dravenChampRadarFake
    },
    {
        img:
            "https://ddragon.leagueoflegends.com/cdn/9.7.1/img/champion/Jinx.png",
        label: "Jinx",
        selected: false,
        data: dravenChampRadarFake
    },
    {
        img:
            "https://ddragon.leagueoflegends.com/cdn/9.7.1/img/champion/Kaisa.png",
        label: "Kai'Sa",
        selected: false,
        data: dravenChampRadarFake
    },
    {
        img:
            "https://ddragon.leagueoflegends.com/cdn/9.7.1/img/champion/Lucian.png",
        label: "Lucian",
        selected: false,
        data: dravenChampRadarFake
    }
];
