import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { ChampRadar, TableStats } from "..";
import { ChampRadarData } from "../../models";
import { Colors } from "../../styles";
import { ITab, tabs } from "./definitions";

const TabbedContainerWrap = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
`;

const TabRow = styled.div`
    display: flex;
    flex-direction: column;

    @media only screen and (min-width: 860px) {
        flex-direction: row;
    }
`;

const Tab = styled.div`
    display: flex;
    align-items: center;
    padding: 10px;
    flex: 1;
    border-left: 1px solid ${Colors.BorderColor};
    border-right: 1px solid ${Colors.BorderColor};
    border-bottom: 1px solid ${Colors.BorderColor};
    cursor: pointer;

    &:first-child {
        border-top: 1px solid ${Colors.BorderColor};
    }

    &:last-child {
        border-bottom-color: ${Colors.LEC1};
    }

    &.selected {
        background-color: ${Colors.LEC1};

        &:first-child {
            border-left: 1px solid ${Colors.LEC1};
        }
    }
    span {
        text-transform: uppercase;
        font-weight: 600;
    }

    img {
        border-radius: 50%;
        height: 35px;
        width: 35px;
        margin-right: 10px;
    }

    @media only screen and (min-width: 860px) {
        border-top: 1px solid ${Colors.BorderColor};
        border-bottom: 1px solid ${Colors.LEC1};
        border-right: 1px solid ${Colors.BorderColor};

        &:first-child {
            border-left: 1px solid ${Colors.BorderColor};
        }

        &.selected {
            border-right: 1px solid ${Colors.LEC1};

            &:first-child {
                border-left: 1px solid ${Colors.LEC1};
            }
        }
    }
`;

const ContentContainer = styled.div`
    border-left: 1px solid ${Colors.LEC1};
    border-bottom: 1px solid ${Colors.LEC1};
    border-right: 1px solid ${Colors.LEC1};
    flex: 1;
    display: grid;
    padding: 20px;
    grid-template-columns: auto;
    grid-template-areas:
        "table"
        "graph";
    grid-gap: 1rem;

    @media only screen and (min-width: 860px) {
        grid-template-columns: 50% auto;
        grid-template-areas: "table graph";
    }

    div.table {
        display: flex;
        flex-direction: column;
    }

    div.radar {
        min-width: 0;
        height: 40vh;
    }
`;

interface TabbedContainerProps {
    data: ChampRadarData[];
}

/**
 * TabbedContainer component
 * Displays content in a tabbed style layout.
 * Should ideally accept an array of children (or something similar) so that
 * rendered content can be passed to it rather than fixed as it is here.
 */
export function TabbedContainer (props: TabbedContainerProps): JSX.Element {
    // Initial state
    const [state, setState] = useState({
        tabs: processTabs(),
        graphData: tabs.find(x => x.selected)!.data
    });

    /**
     * Everytime we receive new props (i.e. from the rest request) we process
     * these and update the data in the table and graph for the selected champ.
     */
    useEffect(() => {
        const processedTabs: ITab[] = processTabs();

        setState({
            tabs: [...processedTabs],
            graphData: tabs.find(x => x.selected)!.data
        });
    }, [props.data]);

    // Small util function to update champ data
    function processTabs (): ITab[] {
        return tabs.map((x, idx) => {
            x.data = props.data[idx];
            return x;
        });
    }

    // Called when we click on a tab. Simply set its state to selected and re-render
    const selectTab = (idx: number): void => {
        state.tabs.forEach(x => (x.selected = false));
        state.tabs[idx].selected = true;

        setState({ tabs: [...state.tabs], graphData: state.tabs[idx].data });
    };

    return (
        <TabbedContainerWrap>
            <TabRow>
                {state.tabs.map((x, idx) => (
                    <Tab
                        key={idx}
                        className={x.selected ? "selected" : ""}
                        onClick={() => selectTab(idx)}
                    >
                        <img src={x.img} />
                        <span>{x.label}</span>
                    </Tab>
                ))}
            </TabRow>
            <ContentContainer>
                <div className="table">
                    <p>
                        Champion specific detailed stats across a pool of
                        measured games.
                    </p>
                    <TableStats data={state.graphData} />
                </div>
                <div className="radar">
                    <ChampRadar data={state.graphData} />
                </div>
            </ContentContainer>
        </TabbedContainerWrap>
    );
}
