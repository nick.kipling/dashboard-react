import React, { useState } from "react";
import styled, { css } from "styled-components";
import { Icon, Timer } from "..";
import { icons } from "../../models";
import { Colors } from "../../styles";

const expandedStyles = css`
    div {
        p.light,
        hr,
        div.info {
            display: block;
        }

        h5 {
            text-transform: uppercase;
            margin-bottom: 0;
        }

        a,
        a:visited {
            color: ${Colors.LEC1};
            text-decoration: none;
            border-bottom: 1px dotted ${Colors.LEC1};
            cursor: pointer;

            &:hover {
                border-bottom: 1px solid ${Colors.LEC1};
            }
        }

        ul {
            padding: 0 0 0 20px;
        }

        ul li,
        p {
            font-weight: normal;
            line-height: 1.6rem;
            font-size: 0.9rem;
        }
    }
`;

const MenuContainer = styled.div`
    display: flex;
    background-color: ${Colors.MenuBG};
    color: ${Colors.MenuFont};
    font-weight: 900;
    padding: 20px;
    width: 8vw;
    height: 100vh;
    overflow-y: auto;
    overflow-x: hidden;
    min-width: 65px;
    transition: width 0.3s ease-in;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 9999;

    &.expanded {
        width: 80vw;
        ${expandedStyles}
    }

    &::-webkit-scrollbar {
        background-color: ${Colors.MenuBG};
        width: 0.7em;
    }

    &::-webkit-scrollbar-thumb:window-inactive,
    &::-webkit-scrollbar-thumb {
        background: #494949;
        border-radius: 10px;
    }

    /* Firefox scrollbar */
    scrollbar-color: #494949 ${Colors.MenuBG};

    div {
        flex: 1;

        p {
            margin: 0;
            font-weight: 400;

            &.light {
                font-weight: 300;
                display: none;
            }
        }

        hr,
        div.info {
            display: none;
        }
    }

    @media only screen and (min-width: 800px) {
        width: 25vw;
        min-width: 220px;
        ${expandedStyles}
    }
`;

const Expand = styled.div`
    height: 60px;
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    flex: 0;

    .back-icon {
        transition: transform 0.3s ease-in;
    }

    &.expanded {
        justify-content: flex-end;
        .back-icon {
            transform: rotate(-180deg);
        }
    }

    @media only screen and (min-width: 800px) {
        justify-content: flex-end;
    }
`;

interface MenuProps {
    loading: boolean;
    onTimerFinish (): void;
}

/**
 * Menu component.
 * The left hand side menu that can be expanded.
 */
export function Menu (props: MenuProps): JSX.Element {
    const [expanded, setExpanded] = useState(false);

    const onExpandClick = (): void => {
        setExpanded(!expanded);
    };

    // Probably a more elegant way to handle the appending of classes. Probably
    // better done with props inside the styled-component.
    return (
        <MenuContainer className={expanded ? "expanded" : ""}>
            <div>
                <Expand
                    onClick={onExpandClick}
                    className={expanded ? "expanded" : ""}
                >
                    <Icon className="back-icon" icon={icons.ChevronRight} />
                </Expand>
                <hr />
                <p className="light">Next refresh</p>
                <Timer
                    expanded={expanded}
                    startFrom={30}
                    loading={props.loading}
                    onTimerFinish={props.onTimerFinish}
                />
                <hr />
                <div className="info">
                    <h5>Resources</h5>
                    <ul>
                        <li>
                            <a
                                href="https://www.typescriptlang.org/"
                                target="_blank"
                            >
                                Typescript
                            </a>
                        </li>
                        <li>
                            <a href="https://reactjs.org/" target="_blank">
                                ReactJS
                            </a>{" "}
                            (using hooks)
                        </li>
                        <li>
                            <a
                                href="https://www.styled-components.com/"
                                target="_blank"
                            >
                                Styled-components
                            </a>
                        </li>
                        <li>
                            <a href="https://www.chartjs.org/" target="_blank">
                                Chart.js
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://github.com/jerairrest/react-chartjs-2"
                                target="_blank"
                            >
                                React-chartjs-2
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://fonts.google.com/specimen/Libre+Franklin"
                                target="_blank"
                            >
                                Libre Franklin Font
                            </a>
                        </li>
                    </ul>

                    <h5>About</h5>

                    <p>
                        A small dashboard built in React using Typescript to
                        show off some front end development. React was chosen as
                        this is the library I know best, but I decided to use
                        hooks and styled-components, neither of which I have
                        used before in production. Both of these seem like
                        interesting approaches, but require some more usage
                        before I can form a fully valid opinion.
                    </p>
                    <br />
                    <p>
                        Once the timer is up, it will make a web request to a
                        Firebase API. This will return some randomly generated
                        mock data and update the graphs.
                    </p>

                    <h5>Time Est.</h5>
                    <p>About 6 hours.</p>

                    <h5>TO DO</h5>
                    <ul>
                        <li>Better responsive layout</li>
                        <li>Animations on layout changes</li>
                        <li>More flexible data models</li>
                        <li>CSS organisation...</li>
                    </ul>
                </div>
            </div>
        </MenuContainer>
    );
}
