import { MostPlayedData, WinLossData } from ".";
import { Colors } from "../styles";
import { ChampRadarData } from "./graph-data";

export const winLossData: WinLossData = {
    labels: ["Ashe", "Draven", "Jinx", "Kai'Sa", "Lucian"],
    datasets: [
        {
            label: "Win",
            backgroundColor: Colors.LEC1,
            borderColor: Colors.LEC1,
            borderWidth: 1,
            hoverBackgroundColor: Colors.LEC1Hover,
            hoverBorderColor: Colors.LEC1Hover,
            data: [52.7, 50.7, 53.47, 47.48, 47.57]
        },
        {
            label: "Loss",
            backgroundColor: Colors.LEC2,
            borderColor: Colors.LEC2,
            borderWidth: 1,
            hoverBackgroundColor: Colors.LEC2Hover,
            hoverBorderColor: Colors.LEC2Hover,
            data: [47.3, 49.3, 46.53, 52.52, 52.43]
        }
    ]
};

export const mostPlayedFake: MostPlayedData = {
    labels: ["Ashe", "Draven", "Jinx", "Kai'Sa", "Lucian"],
    datasets: [
        {
            data: [300, 50, 100, 70, 90],
            backgroundColor: [
                "#01e5bf",
                "#ff5117",
                "#7e61fc",
                "#3395d2",
                "#a41d2d"
            ],
            hoverBackgroundColor: [
                "#01e5bf",
                "#ff5117",
                "#7e61fc",
                "#3395d2",
                "#a41d2d"
            ]
        }
    ]
};

export const asheChampRadarFake: ChampRadarData = {
    labels: [
        "Win Rate",
        "Gold Earned",
        "Kills",
        "Assists",
        "Deaths",
        "Damage Dealt",
        "Damage Taken"
    ],
    datasets: [
        {
            label: "Ashe",
            backgroundColor: "rgba(255, 81, 23, 0.2)",
            borderColor: "#ff5117",
            pointBackgroundColor: "#ff5117",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "#ff5117",
            data: [65, 59, 90, 81, 56, 55, 40]
        },
        {
            label: "Average",
            backgroundColor: "rgba(1, 229, 191, 0.2)",
            borderColor: "rgba(1, 229, 191, 1.000)",
            pointBackgroundColor: "rgba(1, 229, 191, 1.000)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(1, 229, 191, 1.000)",
            data: [60, 60, 60, 60, 60, 60, 60]
        }
    ]
};

export const dravenChampRadarFake: ChampRadarData = {
    labels: [
        "Win Rate",
        "Gold Earned",
        "Kills",
        "Assists",
        "Deaths",
        "Damage Dealt",
        "Damage Taken"
    ],
    datasets: [
        {
            label: "Draven",
            backgroundColor: "rgba(255, 81, 23, 0.2)",
            borderColor: "#ff5117",
            pointBackgroundColor: "#ff5117",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "#ff5117",
            data: [70, 30, 45, 65, 88, 10, 25]
        },
        {
            label: "Average",
            backgroundColor: "rgba(1, 229, 191, 0.2)",
            borderColor: "rgba(1, 229, 191, 1.000)",
            pointBackgroundColor: "rgba(1, 229, 191, 1.000)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(1, 229, 191, 1.000)",
            data: [60, 60, 60, 60, 60, 60, 60]
        }
    ]
};
