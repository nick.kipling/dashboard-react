import React, { useEffect, useState } from "react";
import styled from "styled-components";
import {
    Header,
    Menu,
    MostPlayed,
    TabbedContainer,
    WinLoss
} from "../../components";
import { mostPlayedFake, winLossData } from "../../models";

const ViewContainer = styled.div`
    display: flex;
    height: 100vh;
    width: 100%;
`;

const MainContainer = styled.div`
    padding-left: 8vw;
    display: block;
    flex-direction: column;
    justify-content: flex-start;
    overflow-x: hidden;

    @media only screen and (min-width: 800px) {
        padding-left: 25vw;
    }

    h1 {
        margin: 0;
    }
`;

const GraphContainer = styled.div`
    display: grid;
    padding: 20px;
    grid-template-columns: auto;
    grid-template-areas:
        "one"
        "two"
        "three";
    grid-gap: 1rem;

    @media only screen and (min-width: 900px) {
        grid-template-columns: 50% auto;
        grid-template-areas:
            "one two"
            "three three";
    }

    div.one,
    div.two,
    div.three {
        min-height: 40vh;
        min-width: 0;
    }

    div.one {
        grid-area: one;
    }

    div.two {
        grid-area: two;
    }

    div.three {
        grid-area: three;
        display: flex;
        flex: 1;
    }
`;

/**
 * The main page level component.
 */
export function Main (): JSX.Element {
    // Seed the initial render with our bundled fake data.
    const seed = {
        winLoss: winLossData,
        mostPlayed: mostPlayedFake,
        radars: []
    };

    const [state, setState] = useState({ ...seed, loading: false });

    /**
     * The main fetch data function which fetches from the API. This should
     * really be in it's own class / file somewhere but for time I've included
     * it directly in the component. Does not do any real error handling.
     */
    const fetchData = () => {
        setState({ ...state, loading: true });

        // Add a fake delay so we can actually see something happen
        setTimeout(async () => {
            try {
                const res = await fetch(
                    "https://us-central1-dashboard-server-17f6c.cloudfunctions.net/getData"
                );

                const result = await res.json();
                setState({ ...result, loading: false });
            } catch (ex) {
                // tslint:disable-next-line: no-console
                console.error("Fetch error:", ex);
                setState({ ...state, loading: false });
            }
        }, 1500);
    };

    // Trigger initial fetch of data only once on component first load
    useEffect(fetchData, []);

    return (
        <ViewContainer>
            <Menu loading={state.loading} onTimerFinish={fetchData} />
            <MainContainer>
                <Header />
                <GraphContainer className="single">
                    <div className="one">
                        <WinLoss data={state.winLoss} />
                    </div>
                    <div className="two">
                        <MostPlayed data={state.mostPlayed} />
                    </div>
                    <div className="three">
                        <TabbedContainer data={state.radars} />
                    </div>
                </GraphContainer>
            </MainContainer>
        </ViewContainer>
    );
}
