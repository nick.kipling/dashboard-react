export enum Colors {
    MenuBG = "#050019",
    MenuFont = "#FFF",
    BorderColor = "#e1e1e1",

    LEC1 = "#01e5bf",
    LEC1Hover = "rgba(1, 229, 191, 0.4)",
    LEC2 = "#ff5117",
    LEC2Hover = "rgba(255, 81, 23, 0.4)"
}
