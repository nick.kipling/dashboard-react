# Dashboard React

A small dashboard as per the challenge sent to me. To see the end result, you can either clone the repository and follow the instructions below or go to: https://elastic-wescoff-323a9b.netlify.com

_Note: I've not extensively tested in every browser but it was working fine for me on the latest versions of Chrome, Safari and Firefox._

This is a fake dashboard based off League of Legends champion stats. Most of the data is available via their official API but it's not real time. Therefore, I've faked this data and created a server side endpoint on Firebase that will randomly generate some values for the client to display.

The client currently polls for data updates every 30 seconds. Ideally, if you were using a real server, you'd perhaps not poll and instead set up a websocket connection to update data automatically when available.

I chose React for this as that's what I have the most experience with. I was going to try Vue but didn't think I'd be able to complete it in the time. It turned out that using Hooks / Styled-components took a lot of extra time anyways, so I really should have just gone with Vue from the start.

I tried to keep the layout of the folders and files matching how I'd typically layout one of my applications, but I did have to make some shortcuts to complete within a reasonable time. The style-components usage leaves a little to be desired and is something I'd consider thinking about reworking if I were to enhance further. Also, some of the CSS animations could be improved for better performance.

Likewise, the React hooks and web request functions could also all be improved by being abstracted into dedicated modules so they're easily shared and reused.

Finally, I've also tried to make the page mostly responsive. It's fairly limited and basic in how the responsiveness works as there is only one breakpoint between "mobile" and "desktop". With more time this could be improved a lot further.

---

__Original Readme:__

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
